// Passport JS
const passport = require('passport');
var jwt = require('jsonwebtoken');
var passportJWT = require("passport-jwt");

// Import Logger
const logger = require('../lib/logger');

// Helper function
var helperfunctions = require('../lib/helperfunctions');

// Import configurations
const config = require('../config.json');
var environment = process.env.NODE_ENV || 'development';
environment = environment.trim();

var ExtractJwt = passportJWT.ExtractJwt;
var JwtStrategy = passportJWT.Strategy;

var jwtOptions = {}

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = config[environment].jwtsecretOrKey;

module.exports = passport => {
    passport.use(new JwtStrategy(jwtOptions, function(jwt_payload, done) {
        logger.info('Payload received', jwt_payload);
        
        helperfunctions.gettingemployeedetails(null, function(error, usersInfo) {
            if (error) {
                logger.error(error);
                res.status(500).json({ error: 'Database issue in getting user details for JWT authentication' })
            } else {
                var flag = false;

                try {

                    const signedJWT = jwt.sign(jwt_payload, config[environment].jwtsecretOrKey);

                    // Verify Token of user first 
                    var TokenList = require('../../tokenlist.json');
                    if(Object.keys(TokenList).length != 0 && TokenList[signedJWT].employeeid === jwt_payload.id) {
                        for(let i=0; i<usersInfo.length; i++) {
                            if (usersInfo[i].employeeid == jwt_payload.id) {
                                flag = true;
                                return done(null, usersInfo[i])
                            }
                        }
                    }

                    if(!flag) {

                        for(var key in TokenList) {
                            if (jwt_payload.id == TokenList[key].employeeid) {
                                logger.info("Deleting employeeid as JWT expired : " + TokenList[key].employeeid);
                                delete TokenList[key];
                            }
                        }

                        TokenList = JSON.stringify(TokenList);

                        const fs = require('fs')
                        fs.writeFile('../tokenlist.json', TokenList, 'utf8', (err) => {
                            if (err) {
                            logger.error(err)
                            throw err
                            }
                            logger.info('Deleted token from token list file because of expired JWT');
                        });

                        return done(null, false);
                    }

                } catch (error) {
                    logger.error('Error while verfying user token : ' + error);
                    if(!flag) {
                        return done(null, false);
                    }
                }
            }
        });
        
    }));
};