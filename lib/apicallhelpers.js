var request = require('request');

const logger = require('./logger');

const config = require('../config.json');
var environment = process.env.NODE_ENV || 'development';
environment = environment.trim();

// Function to get all user roles from DB
function getUserRoles (callback) {

    // Set the headers
    var headers = {
        'Content-Type': 'application/json'
    }

    // Configure the request
    var options = { 
        method: 'GET',
        url: 'http://localhost:' + config[environment].port + '/api/users/getroles',
        headers: { 
            'cache-control': 'no-cache',
            'user-agent': 'Server call' },
        json: true 
    };

    // Start the request
    request(options, function (error, response, body) {
        if (!error) {
            // Print out the response body
            logger.debug(JSON.stringify(body));
            callback(null, body);
        }

        if(error) {
            logger.error(error);
            callback(error);
        }
    })

}

// Function to get all direction from DB
function getDirections (callback) {

    // Configure the request
    var options = { 
        method: 'GET',
        url: 'http://localhost:' + config[environment].port + '/api/house/getdirections',
        headers: { 
            'cache-control': 'no-cache',
            'user-agent': 'Server call' },
        json: true 
    };

    // Start the request
    request(options, function (error, response, body) {
        if (!error) {
            // Print out the response body
            logger.debug(JSON.stringify(body));
            callback(null, body);
        }

        if(error) {
            logger.error(error);
            callback(error);
        }
    })

}

module.exports.getUserRoles = getUserRoles;
module.exports.getDirections = getDirections;