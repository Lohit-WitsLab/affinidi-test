const Sequelize = require('sequelize');

const config = require('../config.json');
var environment = process.env.NODE_ENV || 'development';
environment = environment.trim();

// Import Logger
const logger = require('./logger.js');

// Import API call helper
const apiCallHelper = require('./apicallhelpers');
const fs = require('fs')

const DataTypes = require('../node_modules/sequelize/lib/data-types');

// Override timezone formatting for MSSQL
DataTypes.DATE.prototype._stringify = function _stringify(date, options) {
    return this._applyTimezone(date, options).format('YYYY-MM-DD HH:mm:ss.SSS');
};

const sequelize = new Sequelize(config[environment].database, config[environment].dbUserName, config[environment].dbPassWord, {
    host: config[environment].dbHost,
    dialect: config[environment].dbDialect,
    // you can also pass any dialect options to the underlying dialect library
    // - default is empty
    // - currently supported: 'mysql', 'postgres', 'mssql'
    dialectOptions: {
        // requestTimeout: 60000,
        supportBigNumbers: true,
        options: { "requestTimeout": 300000 }
    },
    freezeTableName: true
});

sequelize.authenticate()
.then(() => {
    logger.info('Connection has been established successfully.');

    apiCallHelper.getUserRoles (function(err, data) {
        if (err) {
            logger.error(err);
            process.exit(5);
        }

        data = JSON.stringify(data);
        data = JSON.parse(data);

        var roleList = {};

        for (let i = 0; i < data.message.length; i++) {
            roleList[data.message[i].rolename] = data.message[i].roleid;
        }

        const path = './userroles.json';

        fs.access(path, fs.F_OK, (err) => {
            if (err) {
              logger.info('User Role json not exists..!!!');
            } else {
                // delete userroles json file Synchronously
                fs.unlinkSync(path);
                logger.info('userroles.json file already exists and now deleted.');
            }
          
            roleList = JSON.stringify(roleList);

            fs.writeFile(path, roleList, 'utf8', (err) => {
                if (err) {
                  logger.error(err)
                  throw err
                }
                logger.info('Saved role list to file.');
            });
        });

    });

    apiCallHelper.getDirections (function(err, data) {
        if (err) {
            logger.error(err);
            process.exit(5);
        }

        data = JSON.stringify(data);
        data = JSON.parse(data);

        var directionList = {};

        for (let i = 0; i < data.message.length; i++) {
            directionList[data.message[i].direction] = data.message[i].directionid;
        }

        const path = './directionlist.json';

        fs.access(path, fs.F_OK, (err) => {
            if (err) {
              logger.info('Direction list json not exists..!!!');
            } else {
                // delete userroles json file Synchronously
                fs.unlinkSync(path);
                logger.info('directionlist.json file already exists and now deleted.');
            }
          
            directionList = JSON.stringify(directionList);

            fs.writeFile(path, directionList, 'utf8', (err) => {
                if (err) {
                  logger.error(err)
                  throw err
                }
                logger.info('Saved direction list to file.');
            });
        });

    });

})
.catch(err => {
    logger.error('Unable to connect to the database:', err);
    process.exit(5);
});

module.exports = sequelize;