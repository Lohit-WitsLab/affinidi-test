const logger = require('./logger');
const sequelize = require('./dbconnector');


// Function to get all the employee details from DB
function getemployeedetails (farmid, callback) {

    // Get user information from DB
    var query = 'EXEC uspGetEmployeeDetail @farmid='+farmid;
    logger.info('Query to get all users info from DB : ' + query);

    sequelize.query(query, { type: sequelize.QueryTypes.SELECT }).then(usersInfo => {
        logger.info(JSON.stringify(usersInfo));
        callback(null, usersInfo)
    })
    .catch((err) => {
        logger.error(err, err.message);
        callback(err, null)
    });
}

// Function to get employee details for usertype check
function gettingemployeedetails (farmid, callback) {
    logger.info('Getting Employee Details for User Type check');

    getemployeedetails(farmid, function(error, usersInfo){
        if (error) {
            logger.error(error);
        } else {
           callback(null, usersInfo);
        }
    });

}

module.exports.gettingemployeedetails = gettingemployeedetails;