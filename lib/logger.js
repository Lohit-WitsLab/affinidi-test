const winston = require('winston');
require('winston-daily-rotate-file');

const config = require('../config.json');
var environment = process.env.NODE_ENV || 'development';
environment = environment.trim();

const logger = winston.createLogger({
    level: config[environment].log_level,
    // format: winston.format.json(),
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.json()
    ),
    defaultMeta: { service: config[environment].app_name },
    transports: [
        //
        // - Write to all logs with level `info` and below to `combined.log` 
        // - Write all logs error (and below) to `error.log`.
        //
        new winston.transports.File({ filename: 'Logs/error.log', level: 'error' }),
        new winston.transports.File({ filename: 'Logs/combined.log' }),

        new winston.transports.DailyRotateFile({
            filename: config[environment].app_name + '-%DATE%.log',
            dirname: 'Logs/',
            datePattern: 'YYYY-MM-DD',
            maxFiles: '15d'
        })
    ]
});

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
// 
if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.combine(
            winston.format.timestamp({
                format: 'YYYY-MM-DD HH:mm:ss'
            }),
            winston.format.simple()
        )
    }));
}


module.exports = logger;