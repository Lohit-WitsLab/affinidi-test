# Affinidi-Test

### Running the project

#### Development Mode

- To run the server in Development mode, run the below command in command prompt

    - On Windows CMD

        $ set NODE_ENV=development && node server.js

    - On Git Bash or Terminal

        $ NODE_ENV=development node server.js

**OR**

- Just run the below command on either Windows CMD or Git Bash or Linux terminal

    $ node server.js