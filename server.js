const express = require('express');
const bodyParser = require('body-parser');

// Import configurations
const config = require('./config.json');
var environment = process.env.NODE_ENV || 'development';
environment = environment.trim();

// Import Logger
const logger = require('./lib/logger.js');

// Import routes api
const users = require('./routes/api/users');
const house = require('./routes/api/houses');

// Import DB
const sequelize = require('./lib/dbconnector.js');

// Passport JS
const passport = require('passport');

const app = express();

// Body parser middleware
app.use(bodyParser.json({extended: true, limit: '100MB'}));
app.use(bodyParser.urlencoded({urlencoded: false, extended: true, limit: '100MB'}));

// Initialize Passport
app.use(passport.initialize());

// Passport config
require('./auth/auth')(passport);

// GET method route
app.get('/', (req, res) => res.send('Hello from Affinidi Server..!!'));

// Use Routes
app.use('/api/users', users);
app.use('/api/house', house);

// ALWAYS Keep 404 and 500 as the last route
// 404
app.use(function(req, res, next) {
    logger.error(`404 raised on route ` + req.originalUrl);
    return res.status(404).json({ error: 'Route '+ req.originalUrl +' not found.' });
});

// 500 - Any server error
app.use(function(err, req, res, next) {
    logger.error(`Server error : ` + err);
    return res.status(500).json({ error: err });
});

//Tokenlist for users
const fs = require('fs')
const path = '../tokenlist.json';

var tokenlistflag=false;

fs.access(path, fs.F_OK, (err) => {
  if (err) {
    logger.info('Token List json not exists..!!!');
  } else {
    tokenlistflag=true;
    logger.info('tokenlist.json file already exists....');
  }

  if(!tokenlistflag) {
    fs.writeFile(path, '{}', 'utf8', (err) => {
      if (err) {
        logger.error(err)
        throw err
      }
      logger.info('New token list file created...!!');
    });
  }
});

const port = process.env.PORT || config[environment].port;

const server = app.listen(port, '0.0.0.0', () => logger.info(`Server is running on port ${port}`));

logger.info(`Server will run in below configuration : \n ` + JSON.stringify(config[environment]));
console.log(`Server configuration : \n ` + JSON.stringify(config[environment]));

process.on('SIGINT', () => {
    logger.info('Shutting down Wall\'s Gator API server....');
    server.close(() => {
        logger.debug('Server closed.');
        sequelize.connectionManager.close()
            .then(() => { 
                logger.debug('Database connection closed..') 
                process.exit(0) 
            })
            .catch((err) => {
                logger.error(err)
                process.exit(1)   
            })    
    });
});

